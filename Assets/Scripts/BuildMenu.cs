﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class BuildMenu : Singleton<BuildMenu>
{


    public event EventHandler<TowerBuildEventArgs> BuyTower;
    public class TowerBuildEventArgs
    {
      public  int price;

    }
    void PrepareSlots()
    {
        for (int i = 0; i < slotContainer.childCount; i++)
        {
            Tower tower = new Tower { towerType = (Tower.TowerType)i };
            TowerData.Data towerData = tower.GetTowerData().GetData;
            var slot = slotContainer.GetChild(i);
            slots.Add(slot);
          

            slots[i].Find("Image").GetComponent<Image>().sprite = towerData.Icon;
            slots[i].Find("Price").GetComponent<TextMeshProUGUI>().SetText(towerData.price.ToString());

            slots[i].GetComponent<Button>().onClick.AddListener(() => {

                BuyTower?.Invoke(this,new TowerBuildEventArgs { price = towerData.price });
                placeTower.SetTower(tower); Hide(); 
            });

        }
    }

    private PlaceTower placeTower;
    private List<Transform> slots = new List<Transform>();
    [SerializeField] Transform slotContainer;
    public static void ShowBuildMenu(PlaceTower placeTower)
    {
        Instance.Show(placeTower);
    }

    protected override void AwakeSingleton()
    {
    }

    private void Start()
    {

        PrepareSlots();
        Hide();
    }


    void PutTower(Tower.TowerType towerType)
    {
        print("PutTower" + towerType.ToString());
        Tower tower = new Tower { towerType = towerType };
        var prefab = tower.GetTowerData();
        //Put the tower
        var obj = Instantiate(prefab, placeTower.transform);

        Hide();

    }

    void Show(PlaceTower placeTower)
    {
        this.placeTower = placeTower;
        gameObject.SetActive(true);
        transform.position = placeTower.transform.position;

    }

    void Hide()
    {
        gameObject.SetActive(false);
    }

  
}

