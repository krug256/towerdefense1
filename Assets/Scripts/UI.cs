﻿using System;
using UnityEngine;
using TMPro;
public class UI : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI hp;
    [SerializeField] TextMeshProUGUI gold;
    [SerializeField] TextMeshProUGUI waves;



    public void DisplayWaves(int number)
    {
        waves.SetText("Waves "+number);
    }
    public void DisplayHp(int number)
    {
        hp.SetText("HP " + number);
    }

    public void DisplayGold(int number)
    {
        gold.SetText("Gold "+number);
    }
}
