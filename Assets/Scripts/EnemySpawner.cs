﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] List<GameObject> waypoints;

    [SerializeField] int countEnemy;

    IEnumerator Start()
    {
        var enemyDatas = GameAssets.Instance.enemyDatas;
        countEnemy = Random.Range(60, 100);
        for (int i = 0; i < countEnemy; i++)
        {
            yield return new WaitForSeconds(.4f);
            var e = Enemy.CreateEnemy(transform.position, enemyDatas[Random.Range(0, enemyDatas.Count)]);
            e.SetPath(waypoints);
        }
    }
}
