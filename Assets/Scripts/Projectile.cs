﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    Vector3 lastPos;

    Enemy enemy;

    int damage;
    [SerializeField] float speed = 20f;


    public static void Create(Vector3 spawnPos, Enemy enemy, int damageAmount)
    {
        var p = Instantiate(GameAssets.Instance.bulletPrefab, spawnPos, Quaternion.identity);
        Projectile projectile = p.GetComponent<Projectile>();
        if (projectile == null)
        {
            projectile = p.AddComponent<Projectile>();
        }
        projectile.SetUp(enemy, damageAmount);
    }

    private void SetUp(Enemy enemy, int damage)
    {
        this.enemy = enemy;
        this.damage = damage;
    }

    private void Update()
    {
        if (enemy!=null)
        {
            Vector3 targetPos = enemy.transform.position;

            var moveDir = (targetPos - transform.position).normalized;
            transform.position += moveDir * speed * Time.deltaTime;

            if (Vector3.Distance(transform.position, targetPos) < .1f)
            {
                enemy.TakeDamage(damage);
                Destroy(gameObject);
            }
        }
        else
        {
            Destroy(gameObject);
        }
    
    }
}
