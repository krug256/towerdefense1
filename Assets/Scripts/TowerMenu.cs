﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerMenu : Singleton<TowerMenu>
{
    public event EventHandler<TowerMenuEventArgs> SellTower;
    public class TowerMenuEventArgs
    {
        public int price;
    }
    [SerializeField] Transform slotContainer;

    private PlaceTower placeTower;
    private Tower tower;
    protected override void AwakeSingleton()
    {

        slotContainer.transform.Find("Sell").GetComponent<Button>().onClick.AddListener(Sell);
        slotContainer.transform.Find("Close").GetComponent<Button>().onClick.AddListener(Hide);


        Hide();
    }
    void Sell()
    {
        TowerData.Data data = placeTower.GetTower().GetTowerData().GetData;
        SellTower?.Invoke(this, new TowerMenuEventArgs{price=data.price });
        placeTower.DeleteTower();
        Hide();

    }
    internal static void ShowTowerMenu(PlaceTower placeTower)
    {
        Instance.Show(placeTower);
    }

    void Show(PlaceTower placeTower)
    {
        this.placeTower = placeTower;
        gameObject.SetActive(true);
        transform.position = placeTower.transform.position;

    }

    void Hide()
    {
        gameObject.SetActive(false);
    }


}
