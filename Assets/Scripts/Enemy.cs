﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public static List<Enemy> enemies = new List<Enemy>();

    private HealthSystem health;
    private List<GameObject> waypoints;
    private int currentWaypoint = 0;
    private float lastWaypointSwitchTime;
    private float speed;
    private int damage;
    private int coinsMax, coinsMin;

    private void Update()
    {
        Move();
    }

    public static Enemy CreateEnemy(Vector3 pos, EnemyData data)
    {
        var t = Instantiate(data.GetData.prefab, pos, Quaternion.identity);
        Enemy enemy = t.GetComponent<Enemy>();
        if (enemy == null)
        {
            enemy = t.AddComponent<Enemy>();
        }

        enemy.Init(data);
        enemies.Add(enemy);
        return enemy;

    }
    public static Enemy GetClosestEnemy(Vector3 position, float maxRange)
    {
        Enemy closest = null;
        foreach (Enemy enemy in enemies)
        {
            if (enemy == null)
            {
                //enemies.Remove(enemy);
                continue;
            }
            if (Vector3.Distance(position, enemy.transform.position) <= maxRange)
            {
                if (closest == null)
                {
                    closest = enemy;
                }
                else
                {
                    if (Vector3.Distance(position, enemy.transform.position) < Vector3.Distance(position, closest.transform.position))
                    {
                        closest = enemy;
                    }
                }
            }
        }
        return closest;
    }
    public void TakeDamage(int d)
    {
        health.Damage(d);
    }
    public void SetPath(List<GameObject> waypoints)
    {
        this.waypoints = waypoints;
    }

    void DamageOnPlayer()
    {
        Player.Instance.health.Damage(damage);

    }
    void Death()
    {
        Destroy(gameObject, .1f);
    }
    void Move()
    {
        Vector3 startPosition = waypoints[currentWaypoint].transform.position;
        Vector3 endPosition = waypoints[currentWaypoint + 1].transform.position;
        // поворот
        float angle;
        Vector3 lookDir = (endPosition - transform.position).normalized;
        angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        //-

        float pathLength = Vector3.Distance(startPosition, endPosition);
        float totalTimeForPath = pathLength / speed;
        float currentTimeOnPath = Time.time - lastWaypointSwitchTime;
        gameObject.transform.position = Vector2.Lerp(startPosition, endPosition, currentTimeOnPath / totalTimeForPath);

        if (gameObject.transform.position.Equals(endPosition))
        {
            if (currentWaypoint < waypoints.Count - 2)
            {
                currentWaypoint++;
                lastWaypointSwitchTime = Time.time;

            }
            else
            {
                DamageOnPlayer();
                Death();
            }
        }
    }
    void Init(EnemyData data)
    {
        lastWaypointSwitchTime = Time.time;
        EnemyData.Data data1 = data.GetData;
        health = new HealthSystem(data1.hp);
        speed = data1.speed;
        damage = data1.damage;
        coinsMin = data1.coinsMin;
        coinsMax = data1.coinsMax;
        health.OnDead += Health_OnDead;

        var healthBar = Instantiate(GameAssets.Instance.healthBar, transform.position + new Vector3(0, 0.35f, 0), Quaternion.identity, transform);
        HealthBar bar = healthBar.GetComponent<HealthBar>();
        bar.Setup(health);

    }

    private void Health_OnDead(object sender, System.EventArgs e)
    {
        Player.Instance.Gold += Random.Range(coinsMin, coinsMax);

        Death();
    }
}
