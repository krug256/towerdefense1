﻿using System.Collections.Generic;
using UnityEngine;
[DefaultExecutionOrder(-1)]
public class GameAssets : Singleton<GameAssets>
{

    protected override void AwakeSingleton()
    {
       
    }

    public TowerData tower1;
    public TowerData tower2;
    public TowerData tower3;
    public TowerData tower4;
    //--------
    public GameObject buildMenuPrefab;
    public GameObject bulletPrefab;
    public GameObject healthBar;

    public Sprite defaultSprite ;

    public List<EnemyData> enemyDatas;
}
