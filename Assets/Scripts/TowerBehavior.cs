﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerBehavior : MonoBehaviour
{
    [SerializeField] Transform hed;
    int damage;
    float range;
    float reload;
    float shootTimer;
    private void Update()
    {
        shootTimer -= Time.deltaTime;
        if (shootTimer <= 0)
        {
            shootTimer = reload;
            Enemy enemy = GetClosestEnemy();
            if (enemy != null)
            {

                float angle;
                Vector3 lookDir = (enemy.transform.position - hed.position).normalized;
                angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg;
                angle -= 90;
                hed.rotation = Quaternion.AngleAxis(angle, Vector3.forward);



                Projectile.Create(transform.position, enemy, damage);
            }
        }
    }

    private Enemy GetClosestEnemy()
    {
        Enemy enemy = Enemy.GetClosestEnemy(Player.Castle.position, range);

        if (enemy != null)
        {
            return enemy;
        }
        else
        {
            return Enemy.GetClosestEnemy(transform.position, range);

        }

    }

    public void SetData(TowerData.Data towerData)
    {
        damage = towerData.damage;
        range = towerData.range;
        reload = towerData.reload;

    }
}
