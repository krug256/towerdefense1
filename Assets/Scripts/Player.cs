﻿using UnityEngine;

public class Player : Singleton<Player>
{
    [SerializeField] TowerMenu towerMenu;
    [SerializeField] BuildMenu buildMenu;
    [SerializeField] UI uI;
    [SerializeField] Transform castle;
    public static Transform Castle { get; set; }
    private int gold;
    public HealthSystem health;

    public int Gold
    {
        get
        {
            return gold;
        }
        set
        {
            gold = value;
            uI.DisplayGold(gold);
        }
    }


    private void TowerMenu_SellTower(object sender, TowerMenu.TowerMenuEventArgs e)
    {
        Gold += e.price;
    }

    private void BuildMenu_BuyTower(object sender, BuildMenu.TowerBuildEventArgs e)
    {
        Gold -= e.price;
    }

    private void Start()
    {
        Gold = 1000;
        uI.DisplayGold(gold);
        uI.DisplayHp(health.GetHelth());
    }
    private void Health_OnHealthChanged(object sender, System.EventArgs e)
    {
        uI.DisplayHp(health.GetHelth());
    }
    private void Update()
    {
        
        

    }

    protected override void AwakeSingleton()
    {
        Castle = castle;
        health = new HealthSystem(100);
        health.OnHealthChanged += Health_OnHealthChanged;
        buildMenu.BuyTower += BuildMenu_BuyTower;
        towerMenu.SellTower += TowerMenu_SellTower;
    }
}
