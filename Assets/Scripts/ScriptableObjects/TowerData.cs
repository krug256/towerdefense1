﻿
using UnityEngine;

[CreateAssetMenu(fileName = "TowerData", menuName = "TD/TowerData")]
public class TowerData : ScriptableObject
{
 
    [SerializeField] Data settings;
    public Data GetData { get { return settings; } set { }}






    [System.Serializable]
    public class Data
    {
        public Sprite Icon;
        public GameObject prefab;
        public int price;
        public int damage = 1;
        public float range = 1f;
        public float reload = 1f;

    }

}
