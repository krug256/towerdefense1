﻿using UnityEngine;
[CreateAssetMenu(fileName = "EnemyData", menuName = "TD/EnemyData")]
public class EnemyData : ScriptableObject
{
    [SerializeField] GameObject prefab;
    [SerializeField] int hp = 10;
    [SerializeField] float speed = 1f;
    [SerializeField] int damage = 1;
    [SerializeField] int coinsMax = 1;
    [SerializeField] int coinsMin = 1;

    public Data GetData
    {
        get
        {
            return new Data
            {
                hp = hp,
                damage = damage,
                speed = speed,
                prefab = prefab,
                coinsMax = coinsMax,
                coinsMin = coinsMin
            };
        }
    }
    public struct Data
    {
        public GameObject prefab;
        public int hp;
        public int damage;
        public float speed;
        public int coinsMax;
        public int coinsMin;
    }
}
