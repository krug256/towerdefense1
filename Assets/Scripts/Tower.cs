﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Tower
{ 
    public enum TowerType :int
    {
        tower1=0,
        tower2=1,
        tower3=2,
        tower4=3
    }
    public TowerType towerType;
    public TowerData GetTowerData()
    {
        switch (towerType)
        {
            case TowerType.tower1:
                return GameAssets.Instance.tower1;
            case TowerType.tower2:
                return GameAssets.Instance.tower2;
            case TowerType.tower3:
                return GameAssets.Instance.tower3;
            case TowerType.tower4:
                return GameAssets.Instance.tower4;
            default:
                return GameAssets.Instance.tower1;
        }

    }
  
}


