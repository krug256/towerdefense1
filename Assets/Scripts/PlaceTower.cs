﻿
using System;
using UnityEngine;

public class PlaceTower : MonoBehaviour
{
    Tower tower;

    [SerializeField] private bool IsPlaced;
    GameObject towerObject;
    public void SetTower(Tower tower)
    {
        IsPlaced = true;
        this.tower = tower;
        towerObject = Instantiate(tower.GetTowerData().GetData.prefab, transform);
        towerObject.GetComponent<TowerBehavior>().SetData(tower.GetTowerData().GetData);
    }
    public void DeleteTower()
    {
        IsPlaced = false;
       if (towerObject)Destroy(towerObject);
    }
    public Tower GetTower()
    {
        return tower;
    }
    private void OnMouseUpAsButton()
    {

        if (!IsPlaced)
        {
            BuildMenu.ShowBuildMenu(this);
        }
        else
        {
            TowerMenu.ShowTowerMenu(this);
        }

    }

  
}
